
var iconClose = document.querySelector("#close");
var iconHome = document.querySelector("#home");
var iconSource = document.querySelector("#source");


// Listener for the buttons and load in storage the Settings //
iconClose.addEventListener("click", function () {
    browser.runtime.sendMessage({ action: "toggleUpdate" });
});

iconHome.addEventListener("click", function () {
    let settings={
        theme: document.querySelector("#theme").value,
        customCSS: document.querySelector("#customCSSContainer").value,
        color : document.querySelector("#color").value
    }
    browser.storage.local.set({settings});
    browser.runtime.sendMessage({ action: "openHome" });
});

////


